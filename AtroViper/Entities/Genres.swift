//
//  Genres.swift
//  AtroViper
//
//  Created by MaR on 05.04.2021.
//

import UIKit

protocol GenreInterface {
    
    var id: Int? {get set}
    var name: String? {get set}
    var selected: Bool? {get set}
    
}

class Genre: Decodable, GenreInterface {
    
    var id: Int?
    var name: String?
    var selected: Bool?
    
}

class GenreResponse: Decodable {
    
    var genres: [Genre]?
}
