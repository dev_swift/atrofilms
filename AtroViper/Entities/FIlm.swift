//
//  FIlm.swift
//  AtroViper
//
//  Created by MaR on 16.03.2021.
//

import Foundation
import UIKit


protocol UpcomingFilmInterface {
    
    var title: String? { get set }
    var backdrop_image: Data? { get set }
    var backdrop_path: String? { get set }
}

protocol PopularFilmInterface {
    
    var title: String? { get set }
    var poster_path: String? { get set }
    var backdrop_image: Data? { get set }
    var genre_ids: [Int]? { get set }
    var poster_image: Data? { get set }
    var vote_average: Double? { get set }
    

}


struct Film: Decodable, UpcomingFilmInterface, PopularFilmInterface {
    var backdrop_path: String?
    var backdrop_image: Data?
    var poster_path: String?
    var genre_ids: [Int]?
    var poster_image: Data?
    var vote_average: Double?
    var title: String?
    let adult: Bool?
    let overview: String?
    let release_date: String?
    let id: Int?
    let original_title: String?
    let original_language: String?
    let popularity: Double?
    let vote_count: Int?
    let video: Bool?
  

}

struct FilmsResponse: Decodable {
    
    let page: Int
    let results: [Film]
    let total_results: Int
    let total_pages: Int

}

