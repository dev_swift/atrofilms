//
//  ShapeForUpcomingFilmsPageControlView.swift
//  AtroViper
//
//  Created by MaR on 31.03.2021.
//

import UIKit

protocol ShapeForUpcomingFilmsPageControlViewInterface {
    
    func configure(view: UpcomingFilmsPageControlInterface?)
}

class ShapeForUpcomingFilmsPageControlView: UIView, ShapeForUpcomingFilmsPageControlViewInterface {
    
    
    func configure(view: UpcomingFilmsPageControlInterface?) {
        guard let boundsFrameUpcomingFilmsPageControl = view?.boundsViewUpcomingFilmsPageControl() else {return}
        self.frame = boundsFrameUpcomingFilmsPageControl
        view?.addShapeForUpcomingFilmsPageControl(view: self)
        self.drawShape()
    }

    private func drawShape() {
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = self.bounds
        let path = UIBezierPath()
        path.move(to: CGPoint(x: self.bounds.origin.x, y: self.frame.size.height))
        path.addLine(to: CGPoint(x: self.bounds.origin.x, y: self.frame.size.height/1.5))
        path.addQuadCurve(to: CGPoint(x: self.frame.size.width, y: self.frame.size.height/1.5), controlPoint: CGPoint(x: self.frame.size.width/2, y: self.frame.size.height*1.25))
        path.addLine(to: CGPoint(x: self.frame.size.width, y: self.frame.size.height))
        path.addLine(to: CGPoint(x: self.bounds.origin.x, y: self.frame.size.height))
        shapeLayer.path = path.cgPath
        self.layer.addSublayer(shapeLayer)
        shapeLayer.fillColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
}
