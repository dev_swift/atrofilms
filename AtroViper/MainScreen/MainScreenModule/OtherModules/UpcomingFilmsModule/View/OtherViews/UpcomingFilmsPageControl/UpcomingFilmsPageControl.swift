//
//  UpcomingFilmsPageControl.swift
//  AtroViper
//
//  Created by MaR on 30.03.2021.
//

import UIKit

protocol UpcomingFilmsPageControlInterface {
    
    func configure(viewCollectionView: UpcomingFilmsViewControllerInterface?, film: [UpcomingFilmInterface]?)
   
    func getCurrentFilm(currentFilm: Int?)
    
    func boundsViewUpcomingFilmsPageControl() -> CGRect?
    
    func addShapeForUpcomingFilmsPageControl(view: ShapeForUpcomingFilmsPageControlViewInterface?)
    
    }



class UpcomingFilmsPageControl: UIPageControl, UpcomingFilmsPageControlInterface {
    
    
    func boundsViewUpcomingFilmsPageControl() -> CGRect? {
        return self.bounds
    }
    
    func addShapeForUpcomingFilmsPageControl(view: ShapeForUpcomingFilmsPageControlViewInterface?) {
        self.addSubview(view as! UIView)
    }
    
    
    var shapeForUpcomingFilmsPageControlView: ShapeForUpcomingFilmsPageControlViewInterface = ShapeForUpcomingFilmsPageControlView()
  
    func configure(viewCollectionView: UpcomingFilmsViewControllerInterface?, film: [UpcomingFilmInterface]?) {
        guard let boundsViewCollectionView = viewCollectionView?.boundsViewCollectionView() else {return}
        guard let filmArray = film else {return}
        self.frame = CGRect(x: boundsViewCollectionView.origin.x, y: boundsViewCollectionView.height-boundsViewCollectionView.height/5, width: boundsViewCollectionView.width, height: boundsViewCollectionView.height/5)
        viewCollectionView?.addUpcomingFilmsPageControlInViewCollectionView(view: self)
        self.numberOfPages = filmArray.count
        
        shapeForUpcomingFilmsPageControlView.configure(view: self)
    }
    
    func getCurrentFilm(currentFilm: Int?) {
        guard let film = currentFilm else {return}
        self.currentPage = film
    }
    
}

