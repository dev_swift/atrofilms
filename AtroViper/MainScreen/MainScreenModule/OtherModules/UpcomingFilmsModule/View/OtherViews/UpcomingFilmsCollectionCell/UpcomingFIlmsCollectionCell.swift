//
//  UpcomingFilmsCollectionCell.swift
//  AtroViper
//
//  Created by MaR on 28.03.2021.
//

import UIKit

protocol UpcomingFilmsCollectionCellInterface {
    func configure(film: UpcomingFilmInterface?)
    func viewBoundsCollectionCell() -> CGRect
    func addTitleTextViewInViewCollectionCell(view: FilmTitleTextViewInterface?)
    func addfilmBackdropImageViewInViewCollectionCell(view: FilmBackdropImageViewInterface?)
}

class UpcomingFilmsCollectionCell: UICollectionViewCell, UpcomingFilmsCollectionCellInterface {
    
    var filmTitleTextView: FilmTitleTextViewInterface = FilmTitleTextView()
    var filmBackdropImageView: FilmBackdropImageViewInterface = FilmBackdropImageView()
    
    
    func configure(film: UpcomingFilmInterface?) {
        
        filmBackdropImageView.configure(film: film, viewCollectionCell: self)
        filmTitleTextView.configure(film: film, viewCollectionCell: self)
        
    }
    
    func viewBoundsCollectionCell() -> CGRect {
        return self.contentView.bounds
    }
    
    func addfilmBackdropImageViewInViewCollectionCell(view: FilmBackdropImageViewInterface?) {
        self.contentView.addSubview(view as! UIView)
    }
    
 
    func addTitleTextViewInViewCollectionCell(view: FilmTitleTextViewInterface?) {
        self.contentView.addSubview(view as! UIView)
    }
}

