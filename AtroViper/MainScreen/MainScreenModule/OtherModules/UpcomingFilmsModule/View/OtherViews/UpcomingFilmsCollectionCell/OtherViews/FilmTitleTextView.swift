//
//  FilmTitleTextView.swift
//  AtroViper
//
//  Created by MaR on 29.03.2021.
//

import UIKit

protocol FilmTitleTextViewInterface {
    
    func configure(film: UpcomingFilmInterface?, viewCollectionCell: UpcomingFilmsCollectionCellInterface?)
}

class FilmTitleTextView: DisableTouchTextView, FilmTitleTextViewInterface {
    
    func configure(film: UpcomingFilmInterface?, viewCollectionCell: UpcomingFilmsCollectionCellInterface?) {
        
        guard let frameView = viewCollectionCell?.viewBoundsCollectionCell() else {return}
        self.frame = CGRect(x: Int(frameView.origin.x)+10, y: Int(frameView.size.height-(frameView.size.height/2)), width: Int(frameView.width)-20, height: .zero)
        self.setupView(film: film)
        viewCollectionCell?.addTitleTextViewInViewCollectionCell(view: self)
    }
    
    private func setupView(film: UpcomingFilmInterface?) {
        self.textContainer.maximumNumberOfLines = 2
        self.text = film?.title
        self.textColor = .white
        self.font = UIFont(name: "Montserrat-Bold", size: 20)
        self.textContainer.maximumNumberOfLines = 2
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 15.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        self.backgroundColor = #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.3960784314, alpha: 1)
        self.sizeToFit()
    }
}
