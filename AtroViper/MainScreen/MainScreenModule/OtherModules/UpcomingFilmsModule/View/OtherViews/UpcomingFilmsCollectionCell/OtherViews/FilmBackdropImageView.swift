//
//  FilmBackdropImageView.swift
//  AtroViper
//
//  Created by MaR on 29.03.2021.
//

import UIKit

protocol FilmBackdropImageViewInterface {

func configure(film: UpcomingFilmInterface?, viewCollectionCell: UpcomingFilmsCollectionCellInterface?)
}


class FilmBackdropImageView: UIImageView, FilmBackdropImageViewInterface {
    
    func configure(film: UpcomingFilmInterface?, viewCollectionCell: UpcomingFilmsCollectionCellInterface?) {
                
        self.setupView()
        guard let data = film?.backdrop_image else {return}
        self.image = UIImage(data: data)
        guard let frameViewBounds = viewCollectionCell?.viewBoundsCollectionCell() else {return}
        self.frame = CGRect(x: frameViewBounds.origin.x, y: frameViewBounds.origin.y, width: frameViewBounds.width, height: frameViewBounds.height-(frameViewBounds.height/5))
        viewCollectionCell?.addfilmBackdropImageViewInViewCollectionCell(view: self)
    }
    
    private func setupView() {
        self.layer.cornerRadius = 15.0
        self.layer.masksToBounds = true        
    }
}
