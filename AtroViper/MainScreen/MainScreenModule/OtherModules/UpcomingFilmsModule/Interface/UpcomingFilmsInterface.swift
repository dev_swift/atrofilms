//  
//  UpcomingFilmsInterface.swift
//  AtroViper
//
//  Created by MaR on 25.03.2021.
//
//  This file was generated by I-VIPER Xcode Templates
//  so you can design a more scalable and maintainable
//  design to your iOS projects, see https://github.com/fdorado985/I-VIPER
//

import UIKit

// MARK: - Presenter -> View

protocol UpcomingFilmsViewInterface: class {
    func reloadUpcomingFilms(film: [UpcomingFilmInterface])
}

// MARK: - View -> Presenter

protocol UpcomingFilmsPresenterInterface: class {
    func viewDidload()
    func upcomingFilmsArrayForView()-> [UpcomingFilmInterface]
}

// MARK: - Presenter -> Router

protocol UpcomingFilmsRouterInterface: class {}

// MARK: - Presenter -> Interactor

protocol UpcomingFilmsInteractorInterface: class {
    func getUpcomingFilms(completionHandler: @escaping ([UpcomingFilmInterface]) -> Void)
}
