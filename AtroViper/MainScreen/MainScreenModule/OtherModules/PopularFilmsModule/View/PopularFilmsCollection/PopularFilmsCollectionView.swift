//
//  PopularFilmsCollectionView.swift
//  AtroViper
//
//  Created by MaR on 08.04.2021.
//

import UIKit

protocol PopularFilmsCollectionViewInterface {
    func configure(viewPopularFilmsViewController: PopularFilmsViewControllerInterface?, filtersGenresCollectionView: FiltersGenresCollectionViewInterface?, popularFilms: [PopularFilmInterface]?)
    func reloadPopularFilmsCollectionView()
}

class PopularFilmsCollectionView: UICollectionView, PopularFilmsCollectionViewInterface {
    
    var popularFilmsLocal: [PopularFilmInterface]? = [PopularFilmInterface]()
  
    func configure(viewPopularFilmsViewController: PopularFilmsViewControllerInterface?, filtersGenresCollectionView: FiltersGenresCollectionViewInterface?, popularFilms: [PopularFilmInterface]?) {
    
        self.delegate = self
        self.dataSource = self
                
        popularFilmsLocal = popularFilms
    
        let layout = self.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.scrollDirection = .horizontal
        self.showsHorizontalScrollIndicator = false

        
        self.register(PopularFilmsCollectionCell.self, forCellWithReuseIdentifier: "PopularFilmsCollectionCell")
        
        guard let boundsviewPopularFilmsViewController = viewPopularFilmsViewController?.boundsViewPopularFilmsViewController() else {return}
        
        guard let frameHeightFiltersGenresCollectionView = filtersGenresCollectionView?.frameHeightFiltersGenresCollectionView() else {return}
        self.frame = CGRect(x: boundsviewPopularFilmsViewController.origin.x, y: frameHeightFiltersGenresCollectionView, width: boundsviewPopularFilmsViewController.width, height: boundsviewPopularFilmsViewController.height - frameHeightFiltersGenresCollectionView)
        self.backgroundColor = .white
        viewPopularFilmsViewController?.addViewPopularFilmsCollectionView(viewPopularFilmsCollectionView: self)
        
    }
    
    func reloadPopularFilmsCollectionView() {
        self.reloadData()
    }
    
    
    
}

extension PopularFilmsCollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = popularFilmsLocal?.count else {return 0}
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularFilmsCollectionCell", for: indexPath) as! PopularFilmsCollectionCellInterface
        cell.configure(film: popularFilmsLocal?[indexPath.row])
        return cell as! UICollectionViewCell
    }
    
    
}

extension PopularFilmsCollectionView: UICollectionViewDelegate {
    
}

extension PopularFilmsCollectionView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.size.width/2.4, height: self.frame.height-10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}
