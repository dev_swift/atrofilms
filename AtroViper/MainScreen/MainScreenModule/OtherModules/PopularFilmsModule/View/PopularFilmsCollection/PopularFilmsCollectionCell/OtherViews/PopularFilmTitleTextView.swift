//
//  PopularFilmTitleTextView.swift
//  AtroViper
//
//  Created by MaR on 08.04.2021.
//

import UIKit

protocol PopularFilmTitleTextViewInterface {
    
    func configure(film: PopularFilmInterface?, viewCollectionCell: PopularFilmsCollectionCellInterface?, filmPosterImage: FilmPosterImageViewInterface?)
}

class PopularFilmTitleTextView: DisableTouchTextView, PopularFilmTitleTextViewInterface {
    
    func configure(film: PopularFilmInterface?, viewCollectionCell: PopularFilmsCollectionCellInterface?, filmPosterImage: FilmPosterImageViewInterface?) {
        
        guard let boundsViewCollectionCell = viewCollectionCell?.boundsViewCollectionCell() else {return}
        guard let sizeFilmPosterImage = filmPosterImage?.frameSizeViewFilmPosterImageView() else {return}
        self.setupView()
        self.frame = CGRect(x: boundsViewCollectionCell.origin.x, y: sizeFilmPosterImage.height+15, width: sizeFilmPosterImage.width, height: boundsViewCollectionCell.size.height - sizeFilmPosterImage.height)
        self.text = film?.title
        self.backgroundColor = .white
        viewCollectionCell?.addPopularFilmTitleViewInViewCollectionCell(view: self)
     
    }
    
    private func setupView() {
    
        self.textColor = #colorLiteral(red: 0.231372549, green: 0.231372549, blue: 0.3019607843, alpha: 1)
        self.font = UIFont(name: "Montserrat-Bold", size: 18)
        self.textContainer.maximumNumberOfLines = 4
    }
}
