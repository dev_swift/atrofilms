//
//  PopularFilmsCollectionCell.swift
//  AtroViper
//
//  Created by MaR on 08.04.2021.
//

import UIKit

protocol PopularFilmsCollectionCellInterface {
    
    func configure(film: PopularFilmInterface?)
    func addPopularFilmTitleViewInViewCollectionCell(view: PopularFilmTitleTextViewInterface?)
    func addFilmPosterImageViewInViewCollectionCell(view: FilmPosterImageViewInterface?)
    func boundsViewCollectionCell() -> CGRect

}

class PopularFilmsCollectionCell: UICollectionViewCell, PopularFilmsCollectionCellInterface {
    func addPopularFilmTitleViewInViewCollectionCell(view: PopularFilmTitleTextViewInterface?) {
        return self.contentView.addSubview(view as! UIView)
    }
    
   
    
    var filmPosterImageView: FilmPosterImageViewInterface? = FilmPosterImageView()
    var popularFilmTitleView: PopularFilmTitleTextViewInterface? = PopularFilmTitleTextView()
    

    func boundsViewCollectionCell() -> CGRect {
        return self.contentView.bounds
    }
    
    func addFilmPosterImageViewInViewCollectionCell(view: FilmPosterImageViewInterface?) {
        self.addSubview(view as! UIView)
    }
    
    func configure(film: PopularFilmInterface?) {
        self.contentView.backgroundColor = .white
        filmPosterImageView?.configure(film: film, viewCollectionCell: self)
        popularFilmTitleView?.configure(film: film, viewCollectionCell: self, filmPosterImage: filmPosterImageView)
    }
    
    
}
