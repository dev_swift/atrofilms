//
//  FilmPosterImageView.swift
//  AtroViper
//
//  Created by MaR on 08.04.2021.
//

import UIKit

protocol FilmPosterImageViewInterface {

    func configure(film: PopularFilmInterface?, viewCollectionCell: PopularFilmsCollectionCellInterface?)
    func frameSizeViewFilmPosterImageView()->CGSize
}


class FilmPosterImageView: UIImageView, FilmPosterImageViewInterface {
    func frameSizeViewFilmPosterImageView() -> CGSize {
        return self.frame.size
    }
    
    
    func configure(film: PopularFilmInterface?, viewCollectionCell: PopularFilmsCollectionCellInterface?) {

        self.setupView()
        guard let data = film?.poster_image else {return}
        self.image = UIImage(data: data)
        guard let viewBoundsCollectionCell = viewCollectionCell?.boundsViewCollectionCell() else {return}
        self.frame = CGRect(x: viewBoundsCollectionCell.origin.x, y: viewBoundsCollectionCell.origin.y, width: viewBoundsCollectionCell.width, height: viewBoundsCollectionCell.height-(viewBoundsCollectionCell.height/3))
        viewCollectionCell?.addFilmPosterImageViewInViewCollectionCell(view: self)
 }

    private func setupView() {
        self.layer.cornerRadius = 15.0
        self.layer.masksToBounds = true
    }
}
