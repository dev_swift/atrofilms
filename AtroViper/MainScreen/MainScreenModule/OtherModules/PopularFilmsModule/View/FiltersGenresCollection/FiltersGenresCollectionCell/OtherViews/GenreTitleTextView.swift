//
//  GenreTitleTextView.swift
//  AtroViper
//
//  Created by MaR on 07.04.2021.
//

import UIKit

protocol GenreTitleTextViewInterface {
    
    var genreLocal: GenreInterface? { get set }
    
    func configure(filtersGenresCollectionCell: FiltersGenresCollectionCellInterface?, genre: GenreInterface?)
    func boundsGenreTitleTextView()-> CGRect
    func genreTitle()->String
    func setupView(genre: GenreInterface?)
    func addBackgroundViewInGenreTitleTextView(view: BackgroundViewInterface?)
}

class GenreTitleTextView: DisableTouchTextView, GenreTitleTextViewInterface {
    func addBackgroundViewInGenreTitleTextView(view: BackgroundViewInterface?) {
        self.insertSubview(view as! UIView, at: 0)
    }
    
 
  
    func genreTitle()->String {
        return self.text
    }
    
    func boundsGenreTitleTextView() -> CGRect {
        return self.bounds
    }
    
    
    var genreLocal: GenreInterface?
    var backgroundView: BackgroundViewInterface? = BackgroundView()

    
    func configure(filtersGenresCollectionCell: FiltersGenresCollectionCellInterface?, genre: GenreInterface?) {
        guard let boundsFiltersGenresCollectionCell = filtersGenresCollectionCell?.boundsFiltersGenresCollectionCell() else {return}
        self.frame = boundsFiltersGenresCollectionCell
        backgroundView?.addBackGroundInGenreTitleTextView(view: self)
        genreLocal = genre
        self.setupView(genre: genreLocal)
        filtersGenresCollectionCell?.addViewToFiltersGenresCollectionCell(view: self)
        self.text = genreLocal!.name
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 15
        self.font = UIFont(name: "Montserrat", size: 15)
        self.textAlignment = .center
    }
    
    func setupView(genre: GenreInterface?) {
        
        if genre?.selected == true {
            self.backgroundView?.showView()
            self.textColor = .white
            self.layer.borderWidth = 0.0
        } else {
            self.backgroundView?.hideView()
            self.backgroundColor = .white
            self.textColor = #colorLiteral(red: 0.4470588235, green: 0.3725490196, blue: 1, alpha: 1)
            self.layer.borderColor = #colorLiteral(red: 0.4470588235, green: 0.3725490196, blue: 1, alpha: 1)
            self.layer.borderWidth = 2.0
            self.backgroundColor = .white
        }

    }
    
}

