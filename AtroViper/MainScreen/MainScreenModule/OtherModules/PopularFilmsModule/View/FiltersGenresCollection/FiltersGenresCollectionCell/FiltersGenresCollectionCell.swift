//
//  FiltersGenresCollectionCell.swift
//  AtroViper
//
//  Created by MaR on 02.04.2021.
//

import UIKit


protocol FiltersGenresCollectionCellInterface {
    func configure(genre: GenreInterface?)
    func boundsFiltersGenresCollectionCell()->CGRect
    func addViewToFiltersGenresCollectionCell(view: GenreTitleTextViewInterface?)
    func selectedStatusCell()-> Bool

}

class FiltersGenresCollectionCell: UICollectionViewCell, FiltersGenresCollectionCellInterface {
   
    
    func selectedStatusCell() -> Bool {
        return self.isSelected
    }
    
    var genreTitleTextView: GenreTitleTextViewInterface? = GenreTitleTextView()
        
    func boundsFiltersGenresCollectionCell() -> CGRect {
        return self.bounds
    }
    
    func addViewToFiltersGenresCollectionCell(view: GenreTitleTextViewInterface?) {
        self.contentView.addSubview(view as! UIView)
    }
    
   
    func configure(genre: GenreInterface?) {
        self.contentView.backgroundColor = .white
        guard let genreSelect = genre?.selected else {return}
        self.isSelected = genreSelect
        genreTitleTextView?.configure(filtersGenresCollectionCell: self, genre: genre)
        
    }
}

