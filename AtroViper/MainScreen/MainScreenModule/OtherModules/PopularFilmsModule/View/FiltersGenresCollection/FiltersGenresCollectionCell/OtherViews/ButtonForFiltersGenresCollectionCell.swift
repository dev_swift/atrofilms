//
//  ButtonForFiltersGenresCollectionCell.swift
//  AtroViper
//
//  Created by MaR on 05.04.2021.
//

import UIKit


protocol ButtonForFiltersGenresCollectionCellInterface {
    
    var didSelect: ((Genre) -> Void)? { get set }
    
    func configure(viewFiltersGenresCollectionCell: FiltersGenresCollectionCellInterface?, genres: GenreInterface?)
    func addBackgroundLayerInButtonForFiltersGenresCollectionCell(layer: BackgroundGradientLayerInterface?)
    func boundsViewButtonForFiltersGenresCollectionCell()->CGRect
    
    func test()
    
}



class ButtonForFiltersGenresCollectionCell: UIView, ButtonForFiltersGenresCollectionCellInterface {
    func addBackgroundLayerInButtonForFiltersGenresCollectionCell(layer: BackgroundGradientLayerInterface?) {
        self.layer.insertSublayer(layer as! CALayer, at: 0)
    }
    
    
    func boundsViewButtonForFiltersGenresCollectionCell() -> CGRect {
        return self.bounds
    }
    
//    var backgroundGradientLayer: BackgroundGradientLayerInterface? = BackgroundGradientLayer()
//    var titleGenreForView:
    
    var genre = Genre()
    
    var didSelect: ((Genre) -> Void)?

    
    func configure(viewFiltersGenresCollectionCell: FiltersGenresCollectionCellInterface?, genres: GenreInterface?) {
        guard let boundsViewFiltersGenresCollectionCell = viewFiltersGenresCollectionCell?.boundsFiltersGenresCollectionCell() else {return}
        guard let genres = genres else {return}
        self.frame = boundsViewFiltersGenresCollectionCell
//        viewFiltersGenresCollectionCell?.ad
//        self.setupView(cellIsSelected: <#Bool#>)
        genre = genres as! Genre


    }
    
//    private func setupView(cellIsSelected: Bool) {
//
//        if cellIsSelected == false {
//            backgroundGradientLayer?.removeBackgroundGradientLayerInButtonForFiltersGenresCollectionCell()
//            self.titleLabel?.font = UIFont(name: "Montserrat", size: 15)
//            self.setTitleColor(#colorLiteral(red: 0.4470588235, green: 0.3725490196, blue: 1, alpha: 1), for: .normal)
//            self.layer.masksToBounds = true
//            self.layer.cornerRadius = 15
//            self.layer.borderColor = #colorLiteral(red: 0.4470588235, green: 0.3725490196, blue: 1, alpha: 1)
//            self.layer.borderWidth = 2.0
//            self.backgroundColor = .white
//        } else {
//
//            backgroundGradientLayer?.addBackgroundGradientLayerInButtonForFiltersGenresCollectionCell(view: self)
//            self.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .selected)
//            self.layer.borderWidth = 0.0
//
//        }
//
//    }
//
//    @objc private func changeSelected() {
//        self.isSelected = !self.isSelected
//        setupButton()
//        didSelect?(genre)
//
//    }
//
    func test() {
        print (genre.name)
    }
}


