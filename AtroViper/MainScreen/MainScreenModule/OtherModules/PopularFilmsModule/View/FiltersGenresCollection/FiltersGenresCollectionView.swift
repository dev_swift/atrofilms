//
//  FiltersGenresCollectionView.swift
//  AtroViper
//
//  Created by MaR on 02.04.2021.
//

import UIKit

protocol FiltersGenresCollectionViewInterface {
    func configure(viewPopularFilmsViewController: PopularFilmsViewControllerInterface?, genres: [GenreInterface]?)
    func frameHeightFiltersGenresCollectionView()->CGFloat
}


class FiltersGenresCollectionView: UICollectionView, FiltersGenresCollectionViewInterface {
 
    func frameHeightFiltersGenresCollectionView()->CGFloat {
        return self.frame.size.height
    }
    
    
    var popularFilmsViewController: PopularFilmsViewControllerInterface?
    var genresLocal: [GenreInterface]?

  
    func configure(viewPopularFilmsViewController: PopularFilmsViewControllerInterface?, genres: [GenreInterface]?) {
        
        self.delegate = self
        self.dataSource = self
                
        popularFilmsViewController = viewPopularFilmsViewController
        genresLocal = genres
    
        let layout = self.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.scrollDirection = .horizontal
        self.showsHorizontalScrollIndicator = false
        
        self.register(FiltersGenresCollectionCell.self, forCellWithReuseIdentifier: "FiltersGenresCollectionCell")
        
        guard let boundsviewPopularFilmsViewController = viewPopularFilmsViewController?.boundsViewPopularFilmsViewController() else {return}
        self.frame = CGRect(x: boundsviewPopularFilmsViewController.origin.x, y: boundsviewPopularFilmsViewController.origin.y, width: boundsviewPopularFilmsViewController.width, height: boundsviewPopularFilmsViewController.height/9)
        self.backgroundColor = .white
        viewPopularFilmsViewController?.addViewFiltersGenresCollectionView(viewFiltersGenresCollectionView: self)
    }
        
}

extension FiltersGenresCollectionView: UICollectionViewDataSource {
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = genresLocal?.count else { return 0 }
        return count
    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FiltersGenresCollectionCell", for: indexPath) as! FiltersGenresCollectionCellInterface
        cell.configure(genre: genresLocal?[indexPath.row])
        return cell as! FiltersGenresCollectionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! FiltersGenresCollectionCell
        popularFilmsViewController?.getChangeStatusSelected(view: cell.genreTitleTextView)
        cell.genreTitleTextView?.setupView(genre: cell.genreTitleTextView?.genreLocal)
   
    }
}


extension FiltersGenresCollectionView: UICollectionViewDelegate {}

extension FiltersGenresCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
        let genre = genresLocal?[indexPath.item]
        label.text = genre?.name
        label.font = UIFont(name: "Montserrat-Bold", size: 15)
        label.sizeToFit()
        return CGSize(width: label.frame.width+20, height: label.frame.height+15)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }

}
