//
//  MainScreenScrollView.swift
//  AtroViper
//
//  Created by MaR on 26.03.2021.
//

import UIKit

protocol MainScreenScrollViewInterface {
        
    func configure(mainScreenView: MainScreenViewControllerInterface)
    func addUpcomingFilmsTitleTextView(view: UpcomingFilmsTitleTextViewInterface?)
    func addUpcomingFilmsModuleContainerView(view: UpcomingFilmsModuleContainerViewInterface?)
    func addPopularFilmsTitleTextView(view: PopularFilmsTitleTextViewInterface?)
    func boundsMainScreenScrollView() -> CGRect
    func addPopularFilmsModuleContainerView(view: PopularFilmsModuleContainerViewInterface?)
}


class MainScreenScrollView: UIScrollView, MainScreenScrollViewInterface {
    
    var upcomingFilmsTitleTextView: UpcomingFilmsTitleTextViewInterface? = UpcomingFilmsTitleTextView()
    var upcomingFilmsModuleContainerView: UpcomingFilmsModuleContainerViewInterface? = UpcomingFilmsModuleContainerView()
    var popularFilmsTitleTextView: PopularFilmsTitleTextViewInterface? = PopularFilmsTitleTextView()
    var popularFilmsModuleContainerView: PopularFilmsModuleContainerViewInterface? = PopularFilmsModuleContainerView()

    
    func boundsMainScreenScrollView() -> CGRect {
        return self.bounds
    }
    

    func addUpcomingFilmsTitleTextView(view: UpcomingFilmsTitleTextViewInterface?) {
        self.addSubview(view as! UIView)
    }
    
    func addUpcomingFilmsModuleContainerView(view: UpcomingFilmsModuleContainerViewInterface?) {
        self.addSubview(view as! UIView)
    }
    
    func addPopularFilmsTitleTextView(view: PopularFilmsTitleTextViewInterface?) {
        self.addSubview(view as! UIView)
    }
    
    func addPopularFilmsModuleContainerView(view: PopularFilmsModuleContainerViewInterface?) {
        self.addSubview(view as! UIView)
    }
    
    
    
    func configure(mainScreenView: MainScreenViewControllerInterface) {
        
        let sceneDelegate = UIApplication.shared.connectedScenes.first!.delegate as! SceneDelegate
        let height = sceneDelegate.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        self.frame = (mainScreenView.boundsViewMainScreenVC())
        self.frame.origin.y = height
        self.showsVerticalScrollIndicator = false
        self.contentSize.height = 1000
        mainScreenView.addMainScrollViewInViewMainScreenViewController(view: self)
        upcomingFilmsTitleTextView?.configure(mainScreenScrollView: self)
        upcomingFilmsModuleContainerView?.configure(mainScreenController: mainScreenView, mainScreenScrollView: self, upcomingFilmsTitleTextView: upcomingFilmsTitleTextView!)
        popularFilmsTitleTextView?.configure(mainScreenScrollView: self, upcomingFilmsModuleContainerView: upcomingFilmsModuleContainerView)
        popularFilmsModuleContainerView?.configure(mainScreenController: mainScreenView, mainScreenScrollView: self, popularFilmsTitleTextView: popularFilmsTitleTextView)
        
    }
}
