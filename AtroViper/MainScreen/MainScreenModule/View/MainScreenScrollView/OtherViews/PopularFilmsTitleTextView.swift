//
//  PopularFilmsTitleTextView.swift
//  AtroViper
//
//  Created by MaR on 02.04.2021.
//

import UIKit

protocol PopularFilmsTitleTextViewInterface {
    
    func configure(mainScreenScrollView: MainScreenScrollViewInterface?, upcomingFilmsModuleContainerView: UpcomingFilmsModuleContainerViewInterface?)
    func framePopularFilmsTitleTextView() -> CGRect
}

class PopularFilmsTitleTextView: DisableTouchTextView, PopularFilmsTitleTextViewInterface {
    
    func framePopularFilmsTitleTextView() -> CGRect {
        self.frame
    }
    
    
    func configure(mainScreenScrollView: MainScreenScrollViewInterface?, upcomingFilmsModuleContainerView: UpcomingFilmsModuleContainerViewInterface?) {
        
        guard let boundsMainScreenScrollView = mainScreenScrollView?.boundsMainScreenScrollView() else {return}
        guard let frameUpcomingFilmsModuleContainerView = upcomingFilmsModuleContainerView?.viewUpcomingFilmsModuleContainerView() else {return}
        self.setupView()
        self.frame.origin = CGPoint(x: boundsMainScreenScrollView.origin.x, y: frameUpcomingFilmsModuleContainerView.size.height + frameUpcomingFilmsModuleContainerView.origin.y-1)
        mainScreenScrollView?.addPopularFilmsTitleTextView(view: self)
        self.frame.size.width = frameUpcomingFilmsModuleContainerView.width
    }
    
    private func setupView() {
        self.text = " Фильмы"
        self.backgroundColor = .white
        self.textColor = #colorLiteral(red: 0.231372549, green: 0.231372549, blue: 0.3019607843, alpha: 1)
        self.font = UIFont(name: "Montserrat-Bold", size: 25)
        self.sizeToFit()
    }
}

