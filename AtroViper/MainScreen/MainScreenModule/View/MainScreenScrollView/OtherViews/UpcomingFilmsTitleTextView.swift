//
//  UpcomingFilmsTitleTextView.swift
//  AtroViper
//
//  Created by MaR on 01.04.2021.
//

import UIKit

protocol UpcomingFilmsTitleTextViewInterface {
    
    func configure(mainScreenScrollView: MainScreenScrollViewInterface?)
    func boundsUpcomingFilmsTitleTextView() -> CGRect
}

class UpcomingFilmsTitleTextView: DisableTouchTextView, UpcomingFilmsTitleTextViewInterface {
   
    func boundsUpcomingFilmsTitleTextView() -> CGRect {
        self.bounds
    }
    
    
    func configure(mainScreenScrollView: MainScreenScrollViewInterface?) {
        
        guard let boundsMainScreenScrollView = mainScreenScrollView?.boundsMainScreenScrollView() else {return}
        self.setupView()
        self.frame.origin = CGPoint(x: boundsMainScreenScrollView.origin.x+5, y: boundsMainScreenScrollView.origin.y)
        mainScreenScrollView?.addUpcomingFilmsTitleTextView(view: self)
        
    }
    
    private func setupView() {
        self.text = "Скоро в кино"
        self.backgroundColor = .clear
        self.textColor = .white
        self.font = UIFont(name: "Montserrat-Bold", size: 25)
        self.sizeToFit()
    }
}
