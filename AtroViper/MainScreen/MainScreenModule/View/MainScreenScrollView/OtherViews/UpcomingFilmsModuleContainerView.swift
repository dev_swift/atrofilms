//
//  UpcomingFilmsModuleContainerView.swift
//  AtroViper
//
//  Created by MaR on 26.03.2021.
//

import UIKit

protocol UpcomingFilmsModuleContainerViewInterface {
   
    func configure(mainScreenController: MainScreenViewControllerInterface, mainScreenScrollView: MainScreenScrollViewInterface, upcomingFilmsTitleTextView: UpcomingFilmsTitleTextViewInterface)
    func viewUpcomingFilmsModuleContainerView() -> CGRect
}


class UpcomingFilmsModuleContainerView: UIView, UpcomingFilmsModuleContainerViewInterface {
        
    func viewUpcomingFilmsModuleContainerView() -> CGRect {
        return self.frame
    }
    
    
    func configure(mainScreenController: MainScreenViewControllerInterface, mainScreenScrollView: MainScreenScrollViewInterface, upcomingFilmsTitleTextView: UpcomingFilmsTitleTextViewInterface) {
                
        self.frame = CGRect(x: mainScreenController.boundsViewMainScreenVC().origin.x, y: mainScreenController.boundsViewMainScreenVC().origin.y, width: mainScreenController.boundsViewMainScreenVC().size.width, height: mainScreenController.boundsViewMainScreenVC().size.height/2.5)
        self.frame.origin.y = upcomingFilmsTitleTextView.boundsUpcomingFilmsTitleTextView().size.height
        let upcomingFilmsVC = UpcomingFilmsRouter.createModule()
        mainScreenController.addUpcomingFilmsVC(vc: upcomingFilmsVC)
        upcomingFilmsVC.view.frame = self.bounds
        self.addSubview(upcomingFilmsVC.view)
        mainScreenScrollView.addUpcomingFilmsModuleContainerView(view: self)
    }
}
