//
//  PopularFilmsModuleContainerView.swift
//  AtroViper
//
//  Created by MaR on 02.04.2021.
//

import UIKit

protocol PopularFilmsModuleContainerViewInterface {
    
    func configure(mainScreenController: MainScreenViewControllerInterface?, mainScreenScrollView: MainScreenScrollViewInterface?, popularFilmsTitleTextView: PopularFilmsTitleTextViewInterface?)
}

class PopularFilmsModuleContainerView: UIView, PopularFilmsModuleContainerViewInterface {
    
    func configure(mainScreenController: MainScreenViewControllerInterface?, mainScreenScrollView: MainScreenScrollViewInterface?, popularFilmsTitleTextView: PopularFilmsTitleTextViewInterface?) {
        
        guard let boundsViewMainScreenController = mainScreenController?.boundsViewMainScreenVC() else {return}
        guard let framePopularFilmsTitleTextView = popularFilmsTitleTextView?.framePopularFilmsTitleTextView() else {return}
        
        self.frame = CGRect(x: boundsViewMainScreenController.origin.x, y: boundsViewMainScreenController.origin.y, width: boundsViewMainScreenController.size.width, height: boundsViewMainScreenController.size.height/1.90)
        self.frame.origin.y = framePopularFilmsTitleTextView.height + framePopularFilmsTitleTextView.origin.y
        self.backgroundColor = .red
        mainScreenScrollView?.addPopularFilmsModuleContainerView(view: self)
        let popularFilmsVC = PopularFilmsRouter.createModule()
        popularFilmsVC.view.frame = self.bounds
        self.addSubview(popularFilmsVC.view)
        mainScreenController?.addPopularFilmsVC(vc: popularFilmsVC)
    }
}
