//
//  tabBarViewController.swift
//  AtroViper
//
//  Created by MaR on 17.03.2021.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    var navVC = UINavigationController()
    var text = "tabbar"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTabBar(view: self.view)
        
        let mainVC = MainScreenRouter.createModule()
            
        navVC.addChild(mainVC)
        navVC.navigationBar.isHidden = true
        navVC.tabBarItem = UITabBarItem(title: "Item1", image: nil, selectedImage: nil)
        self.addChild(navVC)
    
        setupTabBar()
    
    }
    
    
    private func configureTabBar(view: UIView) {
        
        let mask = UIView()
            mask.frame = CGRect(x: view.bounds.width/10, y: self.tabBar.bounds.origin.y, width: self.tabBar.bounds.size.width-(view.bounds.width/10)*2, height: self.tabBar.bounds.size.height)
            mask.backgroundColor = .green
            mask.layer.masksToBounds = true
            mask.layer.cornerRadius = 15.0
            self.tabBar.mask = mask
            
        let background = UIView()
            background.frame = mask.frame
            self.tabBar.insertSubview(background, at: 0)
//           
//        let gradient = GradientView()
//            gradient.addGradient(view: background)
//        
    }
    
    private func setupTabBar() {
        
        self.tabBar.tintColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        self.tabBar.unselectedItemTintColor = .white
        self.tabBar.itemPositioning = .centered
        self.tabBar.itemSpacing = CGFloat(10)
//        self.tabBar.items![0].image = #imageLiteral(resourceName: "main")
//        self.tabBar.items![1].image = #imageLiteral(resourceName: "notification")
//        self.tabBar.items![2].image = #imageLiteral(resourceName: "Favorite")
//        self.tabBar.selectedItem?.title = "Главный"
//        self.tabBar.items![0].title = "Главный"
//        self.tabBar.items![1].title = "Уведомления"
//        self.tabBar.items![2].title = "Избранное"
        
    }
    
}
