//
//  BackgroundGradientLayer.swift.swift
//  AtroViper
//
//  Created by MaR on 05.04.2021.
//

import UIKit

protocol BackgroundGradientLayerInterface {
 
}

class BackgroundGradientLayer: CAGradientLayer, BackgroundGradientLayerInterface {
    
    
    private func setupLayer() {
        
        self.startPoint = CGPoint(x: 0, y: 0)
        self.endPoint = CGPoint(x: 1, y: 1)
        let starColor = #colorLiteral(red: 0.2477793236, green: 0.1991117295, blue: 0.942770762, alpha: 1).cgColor
        let endColor = #colorLiteral(red: 0.6470588235, green: 0.4509803922, blue: 1, alpha: 1).cgColor
        self.colors = [starColor,endColor]
       
    }
}
