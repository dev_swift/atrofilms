//
//  BackgroundView.swift
//  AtroViper
//
//  Created by MaR on 17.03.2021.
//

import UIKit

protocol BackgroundViewInterface {
    func addBackGround(view: MainScreenViewControllerInterface?)
    func addBackGroundInGenreTitleTextView(view: GenreTitleTextViewInterface?)
    func hideView()
    func showView()
    
}

class BackgroundView: UIView, BackgroundViewInterface {
    func showView() {
        self.alpha = 1
    }
    
    func hideView() {
        self.alpha = 0
    }
    
    var backgroundGradientLayer: BackgroundGradientLayerInterface? = BackgroundGradientLayer()
    
    func addBackGround(view: MainScreenViewControllerInterface?) {
        
        guard let boundsView = view?.boundsViewMainScreenVC() else {return}
        self.frame = boundsView
        drawBackground()
        view?.addGradientViewInViewMainScreenViewController(view: self)
    }
    
    func addBackGroundInGenreTitleTextView(view: GenreTitleTextViewInterface?) {
        guard let boundsGenreTitleTextView = view?.boundsGenreTitleTextView() else {return}
        self.frame = boundsGenreTitleTextView
        drawBackground()
        view?.addBackgroundViewInGenreTitleTextView(view: self)
        self.alpha = 1
        
    }
    
    private func drawBackground() {
        let gradientlayer = CAGradientLayer()
        gradientlayer.startPoint = CGPoint(x: 0, y: 0)
        gradientlayer.endPoint = CGPoint(x: 1, y: 1)
        let starColor = #colorLiteral(red: 0.2477793236, green: 0.1991117295, blue: 0.942770762, alpha: 1).cgColor
        let endColor = #colorLiteral(red: 0.6470588235, green: 0.4509803922, blue: 1, alpha: 1).cgColor
        gradientlayer.colors = [starColor,endColor]
        gradientlayer.frame = self.bounds
        self.layer.addSublayer(gradientlayer)
    }
}
