//
//  DataService.swift
//  AtroViper
//
//  Created by MaR on 16.03.2021.
//

import Foundation
import UIKit

protocol DataServiceInterface {
    func getUpcomingFilms(completionHandler: @escaping (FilmsResponse?) -> Void)
    func getImage(url: String, completionHandler: @escaping (Data?) -> Void)
    func getGenres(completionHandler: @escaping (GenreResponse?) -> Void)
    func getPopularFfilms(completionHandler: @escaping (FilmsResponse?) -> Void)
    
}

class DataService: DataServiceInterface {
    
    var networkRequest: Request
    var networkDataFetch: DataFetch
    
    init(networkRequest: Request = NetworkRequest(), networkDataFetch: DataFetch = NetworkDataFetch()) {
        self.networkRequest = networkRequest
        self.networkDataFetch = networkDataFetch
    }
    
    func getUpcomingFilms(completionHandler: @escaping (FilmsResponse?) -> Void) {
        networkDataFetch.fetchAndDecodeGenericJSONData(urlString: API.urlString(identifier: .upcomingFilms), completionHandler: completionHandler)
    }
    
    func getImage(url: String, completionHandler: @escaping (Data?) -> Void) {
        networkRequest.request(urlString: url) { (data, error) in
            completionHandler(data)
        }
    }
    
    func getGenres(completionHandler: @escaping (GenreResponse?) -> Void) {
        networkDataFetch.fetchAndDecodeGenericJSONData(urlString: API.urlString(identifier: .genres), completionHandler: completionHandler)
    }
    
    func getPopularFfilms(completionHandler: @escaping (FilmsResponse?) -> Void) {
        networkDataFetch.fetchAndDecodeGenericJSONData(urlString: API.urlString(identifier: .popularFilms), completionHandler: completionHandler)
    }
}
