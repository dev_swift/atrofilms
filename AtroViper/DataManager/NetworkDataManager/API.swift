//
//  API.swift
//  AtroViper
//
//  Created by MaR on 16.03.2021.
//

import Foundation

struct API {
 
    static var baseURL = "https://api.tmdb.org/3/"
    static var baseURLforImage = "http://image.tmdb.org/t/p/"
    private static var apiKey = "api_key=74f73053b98c2cbf285e644e9bf49d34"
    private static var language = "&language=ru-RU"
    private static var region = "&region=ru"
    static var backdrop_sizes = ["w300","w780","w1280","original"]
    static var poster_sizes = ["w92","w154","w185","w342","w500","w780","original"]
    static var profile_sizes = ["w45","w185","h632","original"]
       
    enum Identifier {
        case upcomingFilms
        case genres
        case popularFilms
    }

    static func urlString(identifier: Identifier) -> String {
        var str = baseURL
        switch identifier {

        case .upcomingFilms:
            str += "movie/upcoming?"
        case .genres:
            str += "genre/movie/list?api_key=74f73053b98c2cbf285e644e9bf49d34&language=ru-RU"
        case .popularFilms:
            str += "movie/popular?api_key=74f73053b98c2cbf285e644e9bf49d34&language=ru-RU&page=1&region=ru"
        }
        str += apiKey
        str += language
        str += region
        return str
    }
}

