//
//  NetworkDataFetch.swift
//  AtroViper
//
//  Created by MaR on 16.03.2021.
//

import Foundation

protocol DataFetch {
    func fetchAndDecodeGenericJSONData<T: Decodable>(urlString: String, completionHandler: @escaping (T?) -> Void)
}

class NetworkDataFetch: DataFetch {
    
    var networkRequest: Request
    
    init(networkRequest: Request = NetworkRequest()) {
        self.networkRequest = networkRequest
    }
    
    func fetchAndDecodeGenericJSONData<T: Decodable>(urlString: String, completionHandler: @escaping (T?) -> Void) {
        networkRequest.request(urlString: urlString) { (data, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            let decodedEntity = self.decode(type: T.self, data: data)
            completionHandler(decodedEntity)
        }
    }
    
    private func decode<T: Decodable>(type: T.Type, data: Data?) -> T? {
        let decoder = JSONDecoder()
        guard let data = data else {return nil}
        do {
            let entity = try decoder.decode(type.self, from: data)
            return entity
        } catch {
            print("Error")
            return nil
        }
    }
    
}
