//
//  NetworkRequest.swift
//  AtroViper
//
//  Created by MaR on 16.03.2021.
//

import Foundation

protocol Request {
    func request(urlString: String, completionHandler: @escaping (Data?, Error?) -> Void)
}

class NetworkRequest: Request {
        
    func request(urlString: String, completionHandler: @escaping (Data?, Error?) -> Void) {
        guard let url = URL(string: urlString) else {return}
        let urlRequest = URLRequest(url: url)
        let task = createDataTask(urlRequest: urlRequest, completionHandler: completionHandler)
        task.resume()
        }
    
    private func createDataTask(urlRequest: URLRequest, completionHandler: @escaping (Data?, Error?) -> Void) -> URLSessionDataTask {
        return URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            DispatchQueue.main.async {
                completionHandler(data, error)
            }
        }
    }
}
